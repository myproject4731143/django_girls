
#Внешняя калибровка
#переменные для вызова функций и их действия#
def Ping():
    messagebox.showinfo("GUI Python", "отправляю запрос на " + entry.get() )
    response = os.system("ping -c 4 " + entry.get())
    print (response)
    if response != 0:
        print (response) 
    p = subprocess.run("ping -c 4 " + entry.get(), shell=True, stdout=subprocess.PIPE)
    print(p.stdout.decode())  
    
           
def Navigate():
    messagebox.showinfo("GUI Python", "Вы перешли в ext_calib/scripts" )
    response = os.system(" && bash find_cam_poses.sh  " + entry_bash.get() )
    print("Переход: ~/Workspace/external_calib/scripts")
    p = subprocess.run("", shell=True, stdout=subprocess.PIPE)
    print(p.stdout.decode())   
    
    



def callback():
    global FileAccept
    FileAccept = 1
    print ('FileAccept')
    messagebox.showinfo("GUI Python","Пример: /home/user/Workspace/autopilot/src/n1_bringup/config/n1/052/camera/20230810")
    global dir_path
    dir_path= fd.askdirectory(title="Внешняя калибровка", initialdir="/home/user/Workspace/autopilot/src/n1_bringup/config/n1") 
    print(dir_path)
    
    
    
    
    

def info():
    messagebox.showinfo("GUI Python", "1) Выберите папку с конфигурациями камеры нужного ВАТС  \n \n"
                                      "2) Настройте систему кнопкой 'настроить'  \n \n" 
                                      "3) Скалибруйте внешнюю калибровку и посмотрите результат в выводе " )
    print("Пример: /home/user/Workspace/autopilot/src/")
    print("n1_bringup/config/n1/052/camera/20230810") 
    p = subprocess.run(" " , shell=True, stdout=subprocess.PIPE)
    print(p.stdout.decode())  
    
    
def add_ext_calib():
    global FileAccept
    global Ext_calib_conf 
    if FileAccept == 0:
      messagebox.showinfo("GUI Python","Выберите папку где будет происходить калибровка")
      return
    else: FileAccept == 1
    if len(entry_bash.get()) == 0:
          messagebox.showinfo("GUI Python", "Сначала введите дистанцию!")
          return
    else: len(entry_bash.get()) > 0
    messagebox.showinfo("GUI Python", "Производится настройка перед запуском ...")
    response = os.system( "cp -r ext_calib " + dir_path)
    response = os.system( "cp -r img/front_center.png " + dir_path + "/ext_calib" )
    response = os.system( "cd " + dir_path + "/ext_calib && sed -i '12d' calib_distances.yaml")
    response = os.system( "cd " + dir_path + "/ext_calib && sed -i '11a\  distance: " + entry_bash.get() + "' calib_distances.yaml")
    print("Настройка системы")
    print("ext_calib - ok")
    print("front_center.png - ok")
    print("calib_distances - ok")
    Ext_calib_conf = 1
    p = subprocess.run(" " , shell=True, stdout=subprocess.PIPE)
    print(p.stdout.decode())  
    

    
def find_cam():
    global FileAccept
    global Ext_calib_conf 
    global calibration 
    if FileAccept == 0:
      messagebox.showinfo("GUI Python","Выберите папку где будет происходить калибровка")
      return
    else: FileAccept == 1
    if len(entry_bash.get()) == 0:
          messagebox.showinfo("GUI Python", "Сначала введите дистанцию!")
          return
    else: len(entry_bash.get()) > 0
    if Ext_calib_conf == 0:
          messagebox.showinfo("GUI Python", "Настройте систему")
          return
    else: Ext_calib_conf == 1
    messagebox.showinfo("GUI Python", "Выполняю, Ждите! ... ")
    response = os.system( "cd /home/user/Workspace/external_calib/scripts && bash find_cam_poses.sh " + dir_path )
    print("Выполняется калибровка")
    p = subprocess.run("cd /home/user/Workspace/external_calib/scripts && bash find_cam_poses.sh " + dir_path, shell=True, stdout=subprocess.PIPE)
    response = os.system( "rm -rf " + dir_path + "/ext_calib/front_center.png ")
    calibration = 1
    print(p.stdout.decode())  
    
    
    
    

    
def find_docker():
    global FileAccept
    global Ext_calib_conf 
    global calibration 
    if FileAccept == 0:
      messagebox.showinfo("GUI Python","Выберите папку где будет происходить калибровка")
      return
    else: FileAccept == 1
    if len(entry_bash.get()) == 0:
          messagebox.showinfo("GUI Python", "Сначала введите дистанцию!")
          return
    else: len(entry_bash.get()) > 0
    if Ext_calib_conf == 0:
          messagebox.showinfo("GUI Python", "Настройте систему")
          return
    else: Ext_calib_conf == 1
    messagebox.showinfo("GUI Python", "Запустите в консоле файл Bash start.sh")
    my_file = open("start.sh", "w+")
    my_file.write("#!/bin/bash \n cd /home/devel/Workspace/external_calib/scripts && bash find_cam_poses.sh " + dir_path )
    my_file.close()
    response = os.system("sed -i 's/user/devel/' start.sh && mv start.sh ~/Workspace")
    response = os.system("sed -i 's/user/devel/' /home/user/Workspace/start.sh")
    response = os.system("xterm start pnr ")
    response = os.system("xterm -e docker exec -it pnr bash")
    response = os.system("sed -i 's/user/devel/' /home/user/Workspace/start.sh")
    print("Выполняется калибровка")
    p = subprocess.run("cd /home/user/Workspace/external_calib/scripts && bash find_cam_poses.sh " + dir_path, shell=True, stdout=subprocess.PIPE)
    p = subprocess.run("rm -rf ~/Workspace/start.sh ", shell=True, stdout=subprocess.PIPE)
    calibration = 1
    print(p.stdout.decode())  







def find_cam_result():
    global FileAccept
    global Ext_calib_conf 
    global calibration 
    if calibration == 0:
          messagebox.showinfo("GUI Python", "Нечего проверять, сначала скалибруйте")
          return
    else: calibration == 1
    messagebox.showinfo("GUI Python", "Код этой калибровки в выводе")
    response = os.system( "" )
    print("Результаты текущей калибровки:")
    p = subprocess.run("cat " + dir_path + "/front_center_pose.json", shell=True, stdout=subprocess.PIPE)
    print(p.stdout.decode())  
    
    
def exit():
    messagebox.showinfo("GUI Python", "Все следы работы удалены")
    response = os.system( "rm -rf " + dir_path + "/ext_calib")
    print("выключение")
    p = subprocess.run("", shell=True, stdout=subprocess.PIPE)
    print(p.stdout.decode())  
    sys.exit(0)
    
    
def exit_2():
    messagebox.showinfo("GUI Python", "Кнопка в разработке")
    


    
def find_cam_result_png1():
    global FileAccept
    global Ext_calib_conf 
    global calibration 
    if calibration == 0:
          messagebox.showinfo("GUI Python", "Нечего проверять, сначала скалибруйте")
          return
    else: calibration == 1
    messagebox.showinfo("GUI Python", "Проверьте детекцию!")
    response = os.system( "xdg-open " + dir_path + "/ext_calib/detected_corners/front_center_corners.png" )
    print("Вы проверили калибровку")
    p = subprocess.run("", shell=True, stdout=subprocess.PIPE)
    print(p.stdout.decode())  
    
    
def find_cam_result_png2():
    global FileAccept
    global Ext_calib_conf 
    global calibration 
    if calibration == 0:
          messagebox.showinfo("GUI Python", "Нечего проверять, сначала скалибруйте")
          return
    else: calibration == 1
    messagebox.showinfo("GUI Python", "Проверьте детекцию!")
    response = os.system( "xdg-open " + dir_path + "/ext_calib/detected_corners/front_center_all_corners.png" )
    print("Вы проверили калибровку")
    p = subprocess.run("", shell=True, stdout=subprocess.PIPE)
    print(p.stdout.decode())  



    #МАТ МОДЕЛЬ     
    #переменные для вызова функций и их действия#        
def callback_bag():
    global FileAccept_Mat
    messagebox.showinfo("GUI Python","Для выбора проезда с ssd пишите /media")
    global bag_path
    bag_path= fd.askopenfilename(title="Для выбора проезда с ssd пишите /media", initialdir="/home/user/") 
    FileAccept_Mat = 1
    
    lbInf = Label(tab2, text='###.bag', font=font4, fg = '#408E5D'   )
    lbInf.place(relx=0.28, rely=0.04,  width=80 )
    
    print(bag_path)
    

    

    
def preparation():
    
    global FileAccept_Mat
    global Preparation_file
    
    if FileAccept_Mat == 0:
      messagebox.showinfo("GUI Python","Выберите проезд перед его подготовкой")
      return
    else: FileAccept_Mat == 1
    if len(entryVats.get()) == 0:
          messagebox.showinfo("GUI Python", "Сначала введите номер ВАТС!")
          return
    else: len(entryVats.get()) > 0
    
    
    messagebox.showinfo("GUI Python", "Проезд подготовлен к калибровке")
    response = os.system("cd /home/user/Workspace/bags && mkdir " + entryVats.get())
    print("Создана папка: bags/" + entryVats.get())
    print("Выбранный файл перенесен в папку: " + entryVats.get())
    print("Файл переименован успешно")
    print("Папка "+ entryVats.get() + " перенесена в autopilot",)
    p = subprocess.run("cp -r " + bag_path + " /home/user/Workspace/bags/" + entryVats.get(), shell=True, stdout=subprocess.PIPE)
    p = subprocess.run("cd /home/user/Workspace/bags/" + entryVats.get() + "&& rename 's/Z_steer.e/Z_steering_model_calibration.e/' *.bag", shell=True, stdout=subprocess.PIPE)
    p = subprocess.run("cd /home/user/Workspace/bags/" + entryVats.get() + "&& rename 's/Z_steers.e/Z_steering_model_calibration.e/' *.bag", shell=True, stdout=subprocess.PIPE)
    p = subprocess.run("cd /home/user/Workspace/bags/" + entryVats.get() + "&& rename 's/Z_kinematic.e/Z_kinematic_model_calibration.e/' *.bag", shell=True, stdout=subprocess.PIPE)
    p = subprocess.run("cd /home/user/Workspace/bags/" + entryVats.get() + "&& rename 's/Z_dynamic.e/Z_dynamic_model_calibration.e/' *.bag", shell=True, stdout=subprocess.PIPE)
    p = subprocess.run("cp -r /home/user/Workspace/autopilot-configuration/n1/" + entryVats.get() + " /home/user/Workspace/autopilot/src/n1_bringup/config/n1/", shell=True, stdout=subprocess.PIPE)
    Preparation_file = 1
    print(p.stdout.decode())   
    
    
    
    
    
    
    
    
def callback_bag_steer_front():
    global FileAcceptSteer
    global Preparation_file
    global bag_steers
    if Preparation_file == 0:
      messagebox.showinfo("GUI Python"," Вы еще не подготовили проезд")
      return
    else : Preparation_file == 1
    bag_steers= fd.askopenfilename(title="Выбор Проезда для калибровки steers ", initialdir="/home/user/Workspace/bags")  
    FileAcceptSteer = 1
    
    lbInf_steer = Label(tab2, text='steer.bag', font=font4 ,fg = '#408E5D'   )
    lbInf_steer.place(relx=0.23, rely=0.34,  width=80 )
    print(bag_steers)
    
    
    
    #&& source /opt/ros/melodic/setup.bash && source ~/Workspace/autopilot/devel/setup.bash && DISPLAY=:0 && evo_model_analysis ~/Workspace/bags/037/kalibr-5_n1-037_2023-05-25-09-10-00Z_steering_model_calibration.evo1h_record_default.size_3787262654.bag steer_front
def mathematical_model_calibration_steer_front():
    global FileAcceptSteer
    global Preparation_file
    global bag_steers
    global steer_front_optimize
    global active_calibration
    if Preparation_file == 0:
      messagebox.showinfo("GUI Python"," Вы еще не подготовили проезд")
      return
    else : Preparation_file == 1
    if FileAcceptSteer == 0:
      messagebox.showinfo("GUI Python","Нужно выбрать проезд для калибровки реек!")
      return
    else : FileAcceptSteer == 1
    if len(entryVats.get()) == 0:
          messagebox.showinfo("GUI Python", "Сначала введите номер ВАТС!")
          return
    else: len(entryVats.get()) > 0
    messagebox.showinfo("GUI Python", "Приступаем к калибровке (введите: zsh start.sh)")
    my_file = open("start.sh", "w+")
    my_file.write("#!/bin/bash \n source /opt/ros/melodic/setup.zsh && source ~/Workspace/autopilot/devel/setup.zsh && DISPLAY=:0 && evo_model_analysis " + bag_steers + " steer_front --reparse --optimize")
    my_file.close()
    response = os.system("mv start.sh ~/Workspace")
    response = os.system("sed -i 's/user/devel/' ~/Workspace/start.sh")
    response = os.system("xterm -e docker exec -it pnr zsh")
    
    
    print("Калибровка steers_front успешно завершилась")
    print("Результат калибровки: ")
    p = subprocess.run("rm -rf ~/Workspace/start.sh ", shell=True, stdout=subprocess.PIPE)
    p = subprocess.run("cat /home/user/Workspace/autopilot-configuration/n1/" + entryVats.get() + "/model/optimization_output/steering_model/steer_front_params.yml", shell=True, stdout=subprocess.PIPE)
    steer_front_optimize = 1
    active_calibration = 1
    btn_steer_front = Button(tab2,text='steer_front', bg = '#47A76A' , fg = 'white',
                 command=mathematical_model_calibration_steer_front)
    btn_steer_front.place(relx=0.250, rely=0.421, width=125, anchor="c")
    
    print(p.stdout.decode()) 
    
    

def mathematical_model_calibration_steer_front_reparse():
    global FileAcceptSteer
    global Preparation_file
    global bag_steers
    global steer_front_optimize
    if Preparation_file == 0:
      messagebox.showinfo("GUI Python"," Вы еще не подготовили проезд")
      return
    else : Preparation_file == 1
    if FileAcceptSteer == 0:
      messagebox.showinfo("GUI Python","Нужно выбрать проезд для калибровки реек!")
      return
    else : FileAcceptSteer == 1
    if len(entryVats.get()) == 0:
          messagebox.showinfo("GUI Python", "Сначала введите номер ВАТС!")
          return
    else: len(entryVats.get()) > 0
    if steer_front_optimize == 0:
      messagebox.showinfo("GUI Python","сначала прогоните проезд через оптимизатор")
      return
    else : steer_front_optimize == 1
    
    messagebox.showinfo("GUI Python", "Перезаписываем (введите: zsh start.sh)")
    my_file = open("start.sh", "w+")
    my_file.write("#!/bin/bash \n source /opt/ros/melodic/setup.zsh && source ~/Workspace/autopilot/devel/setup.zsh && DISPLAY=:0 && evo_model_analysis " + bag_steers + " steer_front --reparse")
    my_file.close()
    response = os.system("mv start.sh ~/Workspace")
    response = os.system("sed -i 's/user/devel/' ~/Workspace/start.sh")
    response = os.system("xterm -e docker exec -it pnr zsh")
    print("Данные калибровки steer_front перезаписаны")
    p = subprocess.run("rm -rf ~/Workspace/start.sh ", shell=True, stdout=subprocess.PIPE)
    p = subprocess.run("", shell=True, stdout=subprocess.PIPE)
    
    btn_steer_front_reparse = Button(tab2,text='reparse',bg = '#47A76A' , fg = 'white',
                 command=mathematical_model_calibration_steer_front_reparse)
    btn_steer_front_reparse.place(relx=0.250, rely=0.490, width=125, anchor="c" )
    
    print(p.stdout.decode()) 
    


def mathematical_model_calibration_steer_rear():
    global FileAcceptSteer
    global Preparation_file
    global bag_steers
    global steer_rear_optimize
    global active_calibration 
    if Preparation_file == 0:
      messagebox.showinfo("GUI Python"," Вы еще не подготовили проезд")
      return
    else : Preparation_file == 1
    if FileAcceptSteer == 0:
      messagebox.showinfo("GUI Python","Нужно выбрать проезд для калибровки реек!")
      return
    else : FileAcceptSteer == 1
    if len(entryVats.get()) == 0:
          messagebox.showinfo("GUI Python", "Сначала введите номер ВАТС!")
          return
    else: len(entryVats.get()) > 0
    messagebox.showinfo("GUI Python", "Приступаем к калибровке (введите: zsh start.sh)")
    my_file = open("start.sh", "w+")
    my_file.write("#!/bin/bash \n source /opt/ros/melodic/setup.zsh && source ~/Workspace/autopilot/devel/setup.zsh && DISPLAY=:0 && evo_model_analysis " + bag_steers + " steer_rear --reparse --optimize")
    my_file.close()
    response = os.system("mv start.sh ~/Workspace")
    response = os.system("sed -i 's/user/devel/' ~/Workspace/start.sh")
    response = os.system("xterm -e docker exec -it pnr zsh")
    print("Калибровка steers_front успешно завершилась")
    print("Результат калибровки: ")
    p = subprocess.run("rm -rf ~/Workspace/start.sh ", shell=True, stdout=subprocess.PIPE)
    p = subprocess.run("cat /home/user/Workspace/autopilot-configuration/n1/" + entryVats.get() + "/model/optimization_output/steering_model/steer_rear_params.yml", shell=True, stdout=subprocess.PIPE)
    steer_rear_optimize = 1
    active_calibration = 1
    btn_steer_rear = Button(tab2,text='steer_rear',bg = '#47A76A' , fg = 'white',
                 command=mathematical_model_calibration_steer_rear)
    btn_steer_rear.place(relx=0.750, rely=0.421, width=125, anchor="c" )
    print(p.stdout.decode()) 
    
    
def mathematical_model_calibration_steer_rear_reparse():
    global FileAcceptSteer
    global Preparation_file
    global bag_steers
    global steer_rear_optimize
    if Preparation_file == 0:
      messagebox.showinfo("GUI Python"," Вы еще не подготовили проезд")
      return
    else : Preparation_file == 1
    if FileAcceptSteer == 0:
      messagebox.showinfo("GUI Python","Нужно выбрать проезд для калибровки реек!")
      return
    else : FileAcceptSteer == 1
    if len(entryVats.get()) == 0:
          messagebox.showinfo("GUI Python", "Сначала введите номер ВАТС!")
          return
    else: len(entryVats.get()) > 0
    if steer_rear_optimize == 0:
      messagebox.showinfo("GUI Python","сначала прогоните проезд через оптимизатор")
      return
    else : steer_rear_optimize == 1
    
    messagebox.showinfo("GUI Python", "Перезаписываем (введите: zsh start.sh)")
    my_file = open("start.sh", "w+")
    my_file.write("#!/bin/bash \n source /opt/ros/melodic/setup.zsh && source ~/Workspace/autopilot/devel/setup.zsh && DISPLAY=:0 && evo_model_analysis " + bag_steers + " steer_rear --reparse")
    my_file.close()
    response = os.system("mv start.sh ~/Workspace")
    response = os.system("sed -i 's/user/devel/' ~/Workspace/start.sh")
    response = os.system("xterm -e docker exec -it pnr zsh")
    print("Данные калибровки steer_rear перезаписаны")
    p = subprocess.run("rm -rf ~/Workspace/start.sh ", shell=True, stdout=subprocess.PIPE)
    p = subprocess.run("", shell=True, stdout=subprocess.PIPE)
    
    btn_steer_rear_reparse = Button(tab2,text='reparse',bg = '#47A76A' , fg = 'white',
                 command=mathematical_model_calibration_steer_rear_reparse)
    btn_steer_rear_reparse.place(relx=0.750, rely=0.490, width=125, anchor="c" )
    
    print(p.stdout.decode()) 




def callback_bag_kinematic():
    global bag_kinematic
    global FileAcceptKinematic
    global Preparation_file
    global bag_steers
    global active_calibration
    if Preparation_file == 0:
      messagebox.showinfo("GUI Python"," Вы еще не подготовили проезд")
      return
    else : Preparation_file == 1
    bag_kinematic= fd.askopenfilename(title="Выбор Проезда для калибровки kinematic ", initialdir="/home/user/Workspace/bags")  
    FileAcceptKinematic = 1
    
    lbInf_kinematic = Label(tab2, text='kinematic.bag', font=font4 ,fg = '#408E5D'   )
    lbInf_kinematic.place(relx=0.25, rely=0.675,  width=80 )
    
    print(bag_kinematic)
        
def mathematical_model_calibration_kinematic():
    global FileAcceptKinematic
    global Preparation_file
    global bag_steers
    global kinematic_optimize
    if Preparation_file == 0:
      messagebox.showinfo("GUI Python"," Вы еще не подготовили проезд")
      return
    else : Preparation_file == 1
    if FileAcceptKinematic == 0:
      messagebox.showinfo("GUI Python","Нужно выбрать проезд для калибровки кинематики!")
      return
    else : FileAcceptKinematic == 1
    if len(entryVats.get()) == 0:
          messagebox.showinfo("GUI Python", "Сначала введите номер ВАТС!")
          return
    else: len(entryVats.get()) > 0
   
    messagebox.showinfo("GUI Python", "Приступаем к калибровке (введите: zsh start.sh)")
    my_file = open("start.sh", "w+")
    my_file.write("#!/bin/bash \n source /opt/ros/melodic/setup.zsh && source ~/Workspace/autopilot/devel/setup.zsh && DISPLAY=:0 && evo_model_analysis " + bag_kinematic + " kinematic --reparse --optimize")
    my_file.close()
    response = os.system("mv start.sh ~/Workspace")
    response = os.system("sed -i 's/user/devel/' ~/Workspace/start.sh")
    response = os.system("xterm -e docker exec -it pnr zsh")
    print("Калибровка rbnematic успешно завершилась")
    print("Результат калибровки: ")
    p = subprocess.run("rm -rf ~/Workspace/start.sh ", shell=True, stdout=subprocess.PIPE)
    b = subprocess.run("cat /home/user/Workspace/autopilot-configuration/n1/" + entryVats.get() + "/model/optimization_output/kinematic_model/kinematic_model_params.yml", shell=True, stdout=subprocess.PIPE)
    p = subprocess.run("cat /home/user/Workspace/autopilot-configuration/n1/" + entryVats.get() + "/model/optimization_output/kinematic_model/steer_correction_params.yml", shell=True, stdout=subprocess.PIPE)
    kinematic_optimize = 1
    active_calibration = 1
    btn_kinematic = Button(tab2,text='kinematic',bg = '#47A76A' , fg = 'white',
                 command=mathematical_model_calibration_kinematic)
    btn_kinematic.place(relx=0.250, rely=0.750, width=125, anchor="c" )
    print(p.stdout.decode()) 
    print(b.stdout.decode())
    
    
def mathematical_model_calibration_kinematic_reparse():
    global FileAcceptKinematic
    global Preparation_file
    global bag_steers
    global kinematic_optimize
    if Preparation_file == 0:
      messagebox.showinfo("GUI Python"," Вы еще не подготовили проезд")
      return
    else : Preparation_file == 1
    if FileAcceptKinematic == 0:
      messagebox.showinfo("GUI Python","Нужно выбрать проезд для калибровки кинематики!")
      return
    else : FileAcceptKinematic == 1
    if len(entryVats.get()) == 0:
          messagebox.showinfo("GUI Python", "Сначала введите номер ВАТС!")
          return
    else: len(entryVats.get()) > 0
    if kinematic_optimize == 0:
      messagebox.showinfo("GUI Python","сначала прогоните проезд через оптимизатор")
      return
    else : kinematic_optimize == 1
    messagebox.showinfo("GUI Python", "Перезаписываем (введите: zsh start.sh)")
    my_file = open("start.sh", "w+")
    my_file.write("#!/bin/bash \n source /opt/ros/melodic/setup.zsh && source ~/Workspace/autopilot/devel/setup.zsh && DISPLAY=:0 && evo_model_analysis " + bag_kinematic + " kinematic --reparse")
    my_file.close()
    response = os.system("mv start.sh ~/Workspace")
    response = os.system("sed -i 's/user/devel/' ~/Workspace/start.sh")
    response = os.system("xterm -e docker exec -it pnr zsh")
    print("Данные калибровки kinematic перезаписаны")
    p = subprocess.run("rm -rf ~/Workspace/start.sh ", shell=True, stdout=subprocess.PIPE)
    b = subprocess.run("", shell=True, stdout=subprocess.PIPE)
    btn_kinematic_reparse = Button(tab2,text='reparse',bg = '#47A76A' , fg = 'white',
                 command=mathematical_model_calibration_kinematic_reparse)
    btn_kinematic_reparse.place(relx=0.250, rely=0.820, width=125, anchor="c" )
    print(p.stdout.decode()) 
    print(b.stdout.decode())
    
    
    



def callback_bag_dynamic():
    global bag_dynamic
    global bag_kinematic
    global FileAcceptDynamic
    global Preparation_file
    global bag_steers
    if Preparation_file == 0:
      messagebox.showinfo("GUI Python"," Вы еще не подготовили проезд")
      return
    else : Preparation_file == 1
    bag_dynamic= fd.askopenfilename(title="Выбор Проезда для калибровки dynamic ", initialdir="/home/user/Workspace/bags")  
    FileAcceptDynamic = 1
    
    lbInf_dynamic = Label(tab2, text='dynamic.bag', font=font4 ,fg = '#408E5D'   )
    lbInf_dynamic.place(relx=0.74, rely=0.675,  width=80 )
    
    print(bag_dynamic)
    
        
def mathematical_model_calibration_dynamic():
    global FileAcceptDynamic
    global Preparation_file
    global bag_steers
    global dynamic_optimize
    global active_calibration
    if Preparation_file == 0:
      messagebox.showinfo("GUI Python"," Вы еще не подготовили проезд")
      return
    else : Preparation_file == 1
    if FileAcceptDynamic == 0:
      messagebox.showinfo("GUI Python","Нужно выбрать проезд для калибровки динамики!")
      return
    else : FileAcceptDynamic == 1
    if len(entryVats.get()) == 0:
          messagebox.showinfo("GUI Python", "Сначала введите номер ВАТС!")
          return
    else: len(entryVats.get()) > 0
    
    messagebox.showinfo("GUI Python", "Приступаем к калибровке (введите: zsh start.sh)")
    my_file = open("start.sh", "w+")
    my_file.write("#!/bin/bash \n source /opt/ros/melodic/setup.zsh && source ~/Workspace/autopilot/devel/setup.zsh && DISPLAY=:0 && evo_model_analysis " + bag_dynamic + " dynamic --reparse --optimize")
    my_file.close()
    response = os.system("mv start.sh ~/Workspace")
    response = os.system("sed -i 's/user/devel/' ~/Workspace/start.sh")
    response = os.system("xterm -e docker exec -it pnr zsh")
    print("Калибровка dynamic успешно завершилась")
    print("Результат калибровки: ")
    p = subprocess.run("rm -rf ~/Workspace/start.sh ", shell=True, stdout=subprocess.PIPE)
    p = subprocess.run("cat /home/user/Workspace/autopilot-configuration/n1/" + entryVats.get() + "/model/optimization_output/dynamic_model/dynamic_model_params.yml", shell=True, stdout=subprocess.PIPE)
    dynamic_optimize = 1
    active_calibration = 1
    btn_dynamic = Button(tab2,text='dynamic',bg = '#47A76A' , fg = 'white',
                 command=mathematical_model_calibration_dynamic)
    btn_dynamic.place(relx=0.750, rely=0.750, width=125, anchor="c" )
    print(p.stdout.decode()) 
    

def mathematical_model_calibration_dynamic_reparse():
    global FileAcceptDynamic
    global Preparation_file
    global bag_steers
    global dynamic_optimize
    if Preparation_file == 0:
      messagebox.showinfo("GUI Python"," Вы еще не подготовили проезд")
      return
    else : Preparation_file == 1
    if FileAcceptDynamic == 0:
      messagebox.showinfo("GUI Python","Нужно выбрать проезд для калибровки динамики!")
      return
    else : FileAcceptDynamic == 1
    if len(entryVats.get()) == 0:
          messagebox.showinfo("GUI Python", "Сначала введите номер ВАТС!")
          return
    else: len(entryVats.get()) > 0
    if dynamic_optimize == 0:
      messagebox.showinfo("GUI Python","сначала прогоните проезд через оптимизатор")
      return
    else : dynamic_optimize == 1
    messagebox.showinfo("GUI Python", "Перезаписываем (введите: zsh start.sh)")
    my_file = open("start.sh", "w+")
    my_file.write("#!/bin/bash \n source /opt/ros/melodic/setup.zsh && source ~/Workspace/autopilot/devel/setup.zsh && DISPLAY=:0 && evo_model_analysis " + bag_dynamic + " dynamic --reparse")
    my_file.close()
    response = os.system("mv start.sh ~/Workspace")
    response = os.system("sed -i 's/user/devel/' ~/Workspace/start.sh")
    response = os.system("xterm -e docker exec -it pnr zsh")
    print("Данные калибровки dynamic перезаписаны")
    print("Результат калибровки: ")
    p = subprocess.run("rm -rf ~/Workspace/start.sh ", shell=True, stdout=subprocess.PIPE)
    p = subprocess.run("", shell=True, stdout=subprocess.PIPE)
    btn_dynamic_reparse = Button(tab2,text='reparse',bg = '#47A76A' , fg = 'white',
                 command=mathematical_model_calibration_dynamic_reparse)
    btn_dynamic_reparse.place(relx=0.750, rely=0.820, width=125, anchor="c" )
    print(p.stdout.decode()) 
    
    
    
    
def mathematical_model_calibration_arhive():
    global FileAcceptDynamic
    global Preparation_file
    global bag_steers
    global dynamic_optimize
    global kinematic_optimize
    global steer_rear_optimize
    global steer_front_optimize
    global active_calibration
    
    
    if Preparation_file == 0:
      messagebox.showinfo("GUI Python"," Вы еще не подготовили проезд")
      return
    else : Preparation_file == 1
    
    
    if active_calibration == 0:
      messagebox.showinfo("GUI Python"," Вы еще не успели что либо скалибровать :( ")
      return
    else : active_calibration == 1
    
    messagebox.showinfo("GUI Python", "Калибровки запакованы и перенесены на рабочий стол")
    response = os.system("cd ~/Workspace/autopilot-configuration/n1/" + entryVats.get() + "/model && tar -cf mat_model_" + entryVats.get() + ".tar modeling_output optimization_output")
    response = os.system("mv ~/Workspace/autopilot-configuration/n1/" + entryVats.get() + "/model/mat_model_"+ entryVats.get()+".tar /home/user/Рабочий\ стол")
    print("Все результаты запаковались")
    print("и отправлены на рабочий стол")
    p = subprocess.run("", shell=True, stdout=subprocess.PIPE)
    btn_arhive = Button(tab2,text='Подготовить результаты к отправке',bg = '#47A76A' , fg = 'white',
                 command=mathematical_model_calibration_arhive)
    btn_arhive.place(relx=0.001, rely=0.885, width=400 )
    print(p.stdout.decode()) 
    
    
    
def info_Mat():
    messagebox.showinfo("GUI Python", "1) Выберите папку с конфигурациями камеры нужного ВАТС  \n \n"
                                      "2) Настройте систему кнопкой 'настроить'  \n \n" 
                                      "3) Скалибруйте внешнюю калибровку и посмотрите результат в выводе " )
    print("Пример: /home/user/Workspace/autopilot/src/")
    print("n1_bringup/config/n1/052/camera/20230810") 
    p = subprocess.run(" " , shell=True, stdout=subprocess.PIPE)
    print(p.stdout.decode())  
    
    
def clear():
    
      text.delete("1.0", "end")
    
    
    
#Переменные Математической модели#
FileAcceptSteer = 0
Preparation_file = 0
FileAccept_Mat = 0
FileAcceptKinematic = 0
FileAcceptDynamic = 0
steer_front_optimize = 0
steer_rear_optimize = 0
kinematic_optimize = 0
dynamic_optimize = 0
active_calibration = 0






#Переменные внешней калибровки#
calibration = 0
Ext_calib_conf = 0
FileAccept = 0


#Тестовые переменные#
count = 0
count1 = 0
count3 = 0




#Используемые библиотеки#
from tkinter import *
from tkinter import ttk
from tkinter import *
import tkinter as tk
# -*- coding: utf8 -*-
import os
from tkinter import messagebox
import sys
import subprocess
from tkinter import filedialog as fd 
from tkinter import font


#окно приложения#
win = Tk()
win.geometry(f"400x600+100+200")
win.title('PNRBOX 1.1.8')
win.configure(bg="white")
win.maxsize(400,1000) 
win.resizable(width=False, height=False)

icon = PhotoImage(file = "img/evo.png")
win.iconphoto(False, icon)

#Форматы шрифтов#
font1 = font.Font(family= "Throne and Liberty", size=13, weight="normal", slant="roman")
font2 = font.Font(family= "Throne and Liberty", size=10, weight="normal", slant="roman")
font3 = font.Font(family= "Throne and Liberty", size=15, weight="normal", slant="roman")
font4 = font.Font(family= "Throne and Liberty", size=8, weight="normal", slant="roman")

#Вкладки#
tab_control = ttk.Notebook(win)
tab1 = ttk.Frame(tab_control)
tab2 = ttk.Frame(tab_control)
tab3 = ttk.Frame(tab_control)
tab_control.add(tab1, text='Первая')
tab_control.add(tab2, text='Вторая')
tab_control.add(tab3, text='Вывод')

image = PhotoImage(file='img/evocargo.png')
larger_image = image.zoom(2, 2)         #create a new image twice as large as the original
smaller_image = image.subsample(2, 2)  


img = PhotoImage(file='img/evocargo.png',)
Label(
    win,
    image=img,
    width=15,
    height=10
).place(relx=0.80, rely=0.016, width=70, height=25, anchor="c" )

img2 = PhotoImage(file='img/logo.png',)
Label(
    win,
    image=img2,
    width=15,
    height=10
).place(relx=0.02, rely=0.974, width=30, height=30, anchor="c" )

#Надписи#
lb1 = Label(tab1, text='Внешняя Калибровка',  font=font2)
lb1.place(relx=0.3, rely=0.95, width=150 )
lb2 = Label(tab2, text='Вкладка 2')
lb2.place(relx=0.3, rely=0.95, width=150 )
tab_control.pack(expand=1, fill='both')
lb3 = Label(tab2, text='Математическая модель', font=font2)
lb3.place(relx=0.25, rely=0.95, width=200 )
lb5 = Label(tab1, text='Введите дистанцию', font=font1)
lb5.place(relx=0.20, rely=0.22,  width=250 )
lbVats = Label(tab2, text='Введите Номер ВАТС', font=font2)
lbVats.place(relx=0.20, rely=0.2, width=150, anchor="c" )
lbline = Label(tab2, text='__________________________________________________________________________________', font=font3)
lbline.place(relx=0.0001, rely=0.25, width=400 )
lbline2 = Label(tab2, text='__________________________________________________________________________________', font=font3)
lbline2.place(relx=0.0001, rely=0.85, width=400 )
lbline3 = Label(tab2, text='__________________________________________________________________________________', font=font3)
lbline3.place(relx=0.0001, rely=0.58, width=400 )
lbsteer = Label(tab2, text='Калибровка реек', font=font2)
lbsteer.place(relx=0.508, rely=0.265, width=200, anchor="c" )
lbkinematic_dynamic = Label(tab2, text='Калибровка динамики и кинематики', font=font2)
lbkinematic_dynamic.place(relx=0.500, rely=0.585, width=300, anchor="c" )




#Тесктовые поля#
entryVats = ttk.Entry(tab2,text='Пингануть')
entryVats.place(relx=0.20, rely=0.15, width=150, anchor="c" )
entry_bash = ttk.Entry(tab1,text='Пингануть')
entry_bash.place(relx=0.5, rely=0.35, width=300, anchor="c" )




#Кнопки#
btn_ext_calib = ttk.Button(tab1,text='Настроить', 
       command=add_ext_calib)
btn_ext_calib.place(relx=0.50, rely=0.55, width=150, anchor="c" )


btndir = ttk.Button(tab1,text='Файл', 
       command=callback)
btndir.place(relx=0.15, rely=0.05, width=100, anchor="c" )


btninfo = ttk.Button(tab1,text='Инфо', 
       command=info)
btninfo.place(relx=0.80, rely=0.05, width=80, anchor="c" )

btninfoMat = ttk.Button(tab2,text='Инфо', 
       command=info_Mat)
btninfoMat.place(relx=0.80, rely=0.05, width=80, anchor="c" )



btn_exit_1= ttk.Button(tab1,text='X', 
       command=exit)
btn_exit_1.place(relx=0.95, rely=0.050, width=28, height=28, anchor="c" )

btn_exit_2= ttk.Button(tab2,text='X', 
       command=exit_2)
btn_exit_2.place(relx=0.95, rely=0.050, width=28, height=28, anchor="c" )


btnbag = ttk.Button(tab2,text='Файл', 
       command=callback_bag)
btnbag.place(relx=0.15, rely=0.05, width=100, anchor="c" )


btnbag = ttk.Button(tab2,text='.BAG', 
       command=callback_bag_steer_front)
btnbag.place(relx=0.158, rely=0.355, width=50, anchor="c" )


btnhost = ttk.Button(tab1,text='Калибровка хостом',
                 command=find_cam)
btnhost.place(relx=0.50, rely=0.65, width=300, anchor="c" )

btndocker = ttk.Button(tab1,text='Калибровка Docker',
                 command=find_docker)
btndocker.place(relx=0.50, rely=0.75, width=300, anchor="c" )


btnresult = ttk.Button(tab1,text='Результат',
                 command=find_cam_result)
btnresult.place(relx=0.80, rely=0.90, width=100, anchor="c" )


btnresult_png1 = ttk.Button(tab1,text='Проверка_1',
                 command=find_cam_result_png1)
btnresult_png1.place(relx=0.20, rely=0.90, width=100, anchor="c" )


btnresult_png2 = ttk.Button(tab1,text='Проверка_2',
                 command=find_cam_result_png2)
btnresult_png2.place(relx=0.50, rely=0.90, width=100, anchor="c" )


btn7 = ttk.Button(tab2,text='Подготовить проезд',
                 command=preparation)
btn7.place(relx=0.985, rely=0.120, width=165, anchor="ne")


#Button(tab2, text='OK!',command=mathematical_model_calibration_steer_front,  width=1, bg='green', fg='White').pack(padx=190,pady=228)


btn_steer_front = Button(tab2,text='steer_front', 
                 command=mathematical_model_calibration_steer_front)
btn_steer_front.place(relx=0.250, rely=0.421, width=125, anchor="c")

btn_steer_front_reparse = Button(tab2,text='reparse',
                 command=mathematical_model_calibration_steer_front_reparse)
btn_steer_front_reparse.place(relx=0.250, rely=0.490, width=125, anchor="c" )

btn_steer_rear = Button(tab2,text='steer_rear',
                 command=mathematical_model_calibration_steer_rear)
btn_steer_rear.place(relx=0.750, rely=0.421, width=125, anchor="c" )

btn_steer_rear_reparse = Button(tab2,text='reparse',
                 command=mathematical_model_calibration_steer_rear_reparse)
btn_steer_rear_reparse.place(relx=0.750, rely=0.490, width=125, anchor="c" )



btnbag_kinematic = ttk.Button(tab2,text='.BAG', 
       command=callback_bag_kinematic)
btnbag_kinematic.place(relx=0.158, rely=0.685, width=50, anchor="c" )


btnbag_dynamic = ttk.Button(tab2,text='.BAG', 
       command=callback_bag_dynamic)
btnbag_dynamic.place(relx=0.658, rely=0.685, width=50, anchor="c" )



btn_kinematic = Button(tab2,text='kinematic',
                 command=mathematical_model_calibration_kinematic)
btn_kinematic.place(relx=0.250, rely=0.750, width=125, anchor="c" )

btn_kinematic_reparse = Button(tab2,text='reparse',
                 command=mathematical_model_calibration_kinematic_reparse)
btn_kinematic_reparse.place(relx=0.250, rely=0.820, width=125, anchor="c" )

btn_dynamic = Button(tab2,text='dynamic',
                 command=mathematical_model_calibration_dynamic)
btn_dynamic.place(relx=0.750, rely=0.750, width=125, anchor="c" )

btn_dynamic_reparse = Button(tab2,text='reparse',
                 command=mathematical_model_calibration_dynamic_reparse)
btn_dynamic_reparse.place(relx=0.750, rely=0.820, width=125, anchor="c" )


btn_arhive = Button(tab2,text='Подготовить результаты к отправке',
                 command=mathematical_model_calibration_arhive)
btn_arhive.place(relx=0.001, rely=0.885, width=400 )

#Консолька
#Запись в консоль#
class Redirect():

    def __init__(self, widget,):
        self.widget = widget

    def write(self, text):
        self.widget.insert('end', text)

    #def flush(self):
    #    pass

# --- main ---    

win = tk.Tk()

text = tk.Text(tab3)
text.place(relx=0.0180, rely=0.92999, anchor="sw",width=384, height=520 )

buttonclear = tk.Button(tab3, text='Очистить', command=clear)
buttonclear.place(relx=0.380, rely=0.98999, anchor="sw" )

old_stdout = sys.stdout    
sys.stdout = Redirect(text)















win.mainloop() 


#закрытие кода внутри приложения#



sys.stdout = old_stdout
#свойстыва кнопки 
